# Dependencies
apt-get install -y autoconf libogg-dev libogg0 libvorbis0a libvorbis-dev libvorbisfile3 libvorbisenc2 libsdl-net1.2-dev libsdl-mixer1.2-dev libsdl-sound1.2-dev libsdl-image1.2-dev libsdl-image1.2 libsdl-sound1.2 libsdl-mixer1.2 libsdl-net1.2

# Unpack source
tar xvf dosboxsvn.tgz

# Navigate to the source
cd dosbox

# Configure
./autogen.sh
./configure --enable-core-inline LDFLAGS="-static-libgcc -static-libstdc++ -s" LIBS="-lvorbisfile -lvorbis -logg"

# Build
make

# Copy dependent libraries
cp /lib/x86_64-linux-gnu/* src/.
cp /usr/lib/x86_64-linux-gnu/libSDL* src/.
cp /usr/lib/x86_64-linux-gnu/libvorb* src/.
cp /usr/lib/x86_64-linux-gnu/libogg* src/.
cp /usr/lib/x86_64-linux-gnu/libasound* src/.
cp /usr/lib/x86_64-linux-gnu/* src/.
cp /usr/lib/x86_64-linux-gnu/mesa/* src/.
cp /usr/lib/x86_64-linux-gnu/xorg/* src/.
cp /usr/lib/x86_64-linux-gnu/pulseaudio/* src/.

# Done
cd ..
