import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path   = os.path.dirname(__file__)
emulator_path  = "%s/dosbox/src" % (scripts_path)
binary         = "sh %s/start.sh" % (scripts_path)
job_path       = os.getcwd()

object = Occam.load()

# Open object.json for command line options
# data = object.configuration("General")

# Generate dosconf.conf
# TODO: base it off capabilities and maybe a given metadata section

# Determine path to object run command
# Essentially, dos_executable_path is the input's run command
dos_executable_path = "."
exit_flag = ""

c_drive_path = object.local()
d_drive_path = object.volume()

command = None
exit = False

# Form arguments
args = [binary,
        "-fullscreen",
        "-conf",
        os.path.join(scripts_path, "default.conf")]

# Add mounts for every object
inputs = object.inputs()
for input in inputs:
  path = input.volume()
  command = input._object.get('run', {}).get('command')

  # OCCAM generates mount points for us
  # Pull those out
  mount  = input.mount()
  volume = input.volume()

  local  = input.local()
  output = input.outputPath()

  # Mount writable section of the output to the output mount point (C: usually)
  args.append("-c")
  args.append("\"MOUNT %s %s\"" % (local, output))

  # Mount the object to the mountpoint (D: usually)
  args.append("-c")
  args.append("\"MOUNT %s %s\"" % (mount, volume))

  # Write out the argument later on to exit DOSBox when the command completes
  exit = True

  # Mount all inputs/outputs
  subinputs = input.inputs()
  for subinput in subinputs:
    # Pull out the mountpoints for this object
    mount  = subinput.mount()
    volume = subinput.volume()

    # Mount the object to the mountpoint (D: usually)
    args.append("-c")
    args.append("\"MOUNT %s %s\"" % (mount, volume))

# Write out the command to run
if command:
  # TODO: handle working directory
  args.append("-c")
  args.append("D:")
  args.append("-c")
  args.append("\"%s\"" % (command))

if exit:
  args.append("-c")
  args.append("exit")
  args.append("-exit")

# Form command line
command = ' '.join(args)

# Tell OCCAM how to run DRAMSim2
Occam.report(command, env={"LD_LIBRARY_PATH": emulator_path})
