#!/bin/sh

ROOT=$(readlink -f $(dirname $0))
${ROOT}/dosbox/src/dosbox "$@"

if [ $? -ne 0 ]; then
  ${ROOT}/dosbox/src/dosbox "$@"
fi

exit
